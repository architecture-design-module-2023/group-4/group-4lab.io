# ¿Dónde está Fluffymon?

Un servicio que describe las mascotas perdidas, las recompensas de mascotas (negociadas/gestionadas por el servicio) y los puntos de datos de ubicación (GPS) de los avistamientos de mascotas utilizando la realidad aumentada.
" Dónde está Fluffymon?" es una aplicación móvil dedicada a encontrar mascotas perdidas. Esta aplicación proporciona una herramienta poderosa y efectiva para encontrar mascotas perdidas, al combinar tecnologías como la realidad aumentada, el GPS y la colaboración de la comunidad. El bienestar de las mascotas es la prioridad, y la aplicación se esfuerza por brindar una experiencia confiable y exitosa.

## Autores

- Dario Falconi
- Gabriela Farinango
- Gisela Osorio
- José Luis Sanchez

## Usuarios

- Docenas de dueños de mascotas desaparecidas.
- Cientos de "observadores" (inicialmente).
- Podría ampliarse dependiendo del éxito de la implementación.

## Funcionalidades

- Registro de usuarios interesados en encontrar mascotas.
- Visualización de una lista de mascotas perdidas cerca de la ubicación del usuario.
- Publicación de mensajes de "mascota encontrada" por parte de los buscadores, con prueba fotográfica obligatoria.
- Confirmación de los dueños de mascotas y cobro de recompensas por parte de los buscadores.
- Comentarios en las entradas de mascotas perdidas, donde los usuarios pueden ofrecer puntos de datos adicionales.
- Accesibilidad a través de dispositivos móviles.

## Tecnologías utilizadas

### Programacion
- .Net
- Flutter
### Base datos
- Sql sever
### Herraminetas para la integracion continua y despliegue
- Amazon
- Jenkis
- Bitrise
- Docker
- Git
### Diseño de pantallas
- Figma


## Decisiones de arquitectura

La documentación de decisiones de arquitectura se encuentra en la carpeta "adr" (Architecture Decision Records) y contiene los siguientes documentos:

- [Compatibilidad con android y ios](adr/No_Funcional/adr-1.md)
- [Usabilidad](adr/No_Funcional/adr-2.md)
- [Tiempo de Respuesta](adr/No_Funcional/adr-3.md)
- [Consumo de recursos](adr/No_Funcional/adr-4.md)

- [Registro de usuarios](adr/Funcional/adr-1.md)
- [Publicación de mascota perdida](adr/Funcional/adr-2.md)
- [Lista de publiacaciones de mascotas perdidas](adr/Funcional/adr-3.md)
- [Interfaz de usuario](adr/Funcional/adr-4.md)


## Diagramas

Los diagramas se encuentran en la carpeta "Diagrama" y utilizan el modelo C4 para proporcionar diferentes niveles de abstracción:

- [Diagramas](Diagramas/index.md)
- [Fuente C4 diagramas .dsl](Diagramas/diagrmas-c4.dsl)
- [Contexto](Diagramas/ImagenesC4/structurizr-SystemContext.png)
- [Contenedores](Diagramas/ImagenesC4/structurizr-ContainerLevel.png)
- [Componentes](Diagramas/ImagenesC4/structurizr-APIAplication.png)

## Tablero Miro

Detalle de lo trabajado en el Miro 

- [Requerimientos](Tablero/Grupo_4_Requerimientos.jpg)
- [ADR](Tablero/Grupo_4_ADRs.jpg)
- [Diagrama Arquitectura](Tablero/Grupo_4_Diagrama_Arquitectura.jpg)