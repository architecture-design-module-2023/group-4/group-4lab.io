# GRUPO 4 - Diagramas

## Diagrama de contexto

![Diagrama de contexto](ImagenesC4/structurizr-SystemContext.png "Diagrama de contexto")

![Diagrama de contexto](ImagenesC4/structurizr-SystemContext-key.png "Diagrama de contexto")

## Diagrama de contenedor

![Diagrama de contenedores](ImagenesC4/structurizr-ContainerLevel.png "Diagrama de contenedores")

![Diagrama de contenedores](ImagenesC4/structurizr-ContainerLevel-key.png "Diagrama de contenedores")

## Diagrama de componente

![Diagrama de componente](ImagenesC4/structurizr-APIAplication.png "Diagrama de componente")

![Diagrama de componente](ImagenesC4/structurizr-APIAplication-key.png "Diagrama de componente")

