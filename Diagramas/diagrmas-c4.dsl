workspace  "C4 Model Group 4" "Aplicacion para encontrar mascotas perdidas. App where's Fluffymon?"{

    model {
        /* Actors */
        duenoMascota = person "Dueño de mascota perdida" "Realiza publicaciones y comentarios sobre mascotas perdidas"
        encuentraMascota = person "Persona que encuentra mascota" "Realiza publicaciones y comentarios sobre mascotas encontradas"
        
 
        /* Software Systems,containers and components */
        appSoftwareSystem = softwareSystem "App where's Fluffymon?" "Permite a los usuarios registrarse, publicar mascotas perdidas, ver lista de mascotas y comentar publicaciones"{

            appUsuario = container "Aplicacion movil" "Provee toda la funcionalidad para que los usuarios puedan realizar sus publicaciones." "Flutter" "Aplication"
            tokenSeguridadAplicacion = container "Seguridad - Tokens- OAuth2" "Token de seguridad para las peticiones de las api´s" "OAuth2" "Software System"
            apiAplicacion = container "API Aplicación" "Expone metodos parar la realizacion de solicitudes HTTPS con respuestas JSON para la conexion de la aplicacion." "Contendor :  .Net" "API"{
                apiSeguridad = component "Middleware - OAuth2 " "Controla las peticiones por medio de un token para ser autorizadas" "Token OAuth2" "Software System"
                apiRegistro = component "Controlador - Usuario" "Controla el registro de usuarios en el aplicatico" "MVC - RestController" "Software System"
                apiPublicaciones = component "Controlador - Publicaciones" "Controla las publicaciones de mascotas perdidas" "MVC - RestController" "Software System"
                apiListaMascostas = component "Controlador - Lista Mascotas" "Muestra  lista de publicaciones de mascotas perdias y encontrdas" "MVC - RestController" "Software System"
                apiForoMascotas = component "Controlador - Foto de Mascotas encontradas y avistadas" "Controla el foro de las mascotas avistadas y econtradas" "MVC - RestController" "Software System"
                apiOrm = component "ORM - Entity Framework" "Realiza la comunicacion de la capa de negocio con la capa de datos" "MVC - RestController" "Software System"
                apiIntegacionPasarelasPagos = component "Controlados - Pasarela de Pago" "Realiza la integracion con la pasalela de pago [PayPay/Kushki]" "MVC - RestController" "Software System"
                apiIntegacionRedesSociales = component "Controlados - Redes Sociales" "Realiza la integracion con las Api´s proporcionadas por las redes sociales para hacer las publicaciones" "MVC - RestController" "Software System"
                apiIntegacionAmazonS3Bucket = component "Controlados - Amazon S3 Bucket" "Realiza la integracion con las Api´s proporcionadas por amazon para guadar y obtener las imagenes resgitradas desde el aplicativo" "MVC - RestController" "Software System"
            }
            swaggerAplicacion = container "Documentación" "Documentar Endpoint de los metodos proporcionados en el Api" "Swagger" "Software System"
            baseDatos = container "Base de Datos" "Almacena la información de los usuarios, publicaciones y rutas de las fotos publicadas en la aplicación" "Contendor :  SqlServer" "DataBase"
        }
        
        
        publicidadApp = softwareSystem "Publicidad" "Contiene anuncios de tiendas de mascotas integrando AdMob de Google" "ExternalSystem"
           
        pasarelasPagoApp = softwareSystem "Pasarelas de pagos" "Uso de pasarelas de pago para la gestion de las recompensas ofrecidas" "ExternalSystem"
        
        serviciosAmazon = softwareSystem "Servicios de Amazon" "Uso de servicios de amazon para el alojamineto de la base datos y el api" "ExternalSystem"
        
        tiendaAplicaciones = softwareSystem "Tienda de aplicaciones" "Uso de tiendas para que los ususarios otengan la aplicacion en Andoid o Ios" "ExternalSystem"

        apiRedesSolialesApp = softwareSystem "Redes sociales" "Integracion con redes sociales para amplicar el sistema de publicaciones" "ExternalSystem"{
            apiExternasRedesSociales = container "Registro de usaurio" "Endpoint permite al usuario registrarse en la plataforma" 
        }

        /*Context level connections*/
        duenoMascota -> appSoftwareSystem "Realiza las publicaciones" 
        encuentraMascota -> appSoftwareSystem "Responde las publicaciones"
        publicidadApp -> appSoftwareSystem "Ganancias por anuncios"
        appSoftwareSystem -> pasarelasPagoApp "Se realizan los pagos de recompensas usando"
        appSoftwareSystem -> apiRedesSolialesApp "Integracion con redes sociales"
        appSoftwareSystem -> serviciosAmazon "Alojamiento de la infraestructura" 
        appSoftwareSystem -> tiendaAplicaciones "Descargar aplicacion por medio de" 

        /*Container level connections*/
        duenoMascota -> appUsuario "Utiliza la aplicacion para publicar busquedas de mascotas"
        encuentraMascota -> appUsuario "Utiliza la aplicacion para publicar mascotas encontradas"
        appUsuario -> apiAplicacion "Realiza petiones (GET, POST, UPDATE, DELETE, PATCH) a la API"
        tokenSeguridadAplicacion -> apiAplicacion "Implementacion de"
        swaggerAplicacion -> apiAplicacion "Implementacion de"
        apiAplicacion -> baseDatos "Lectura y escritura de datos hacia - [protocolo TCP/IP]"
        apiAplicacion -> apiRedesSolialesApp "Envia las publicaciones usando los Endpoint porpocionados en la documentacion del api externo"
        publicidadApp -> apiAplicacion "Gestionar las credenciales porpocionadas por AdMod"
        publicidadApp -> appUsuario "Instalar dependencias a nivel de aplicacion para el usar"
        pasarelasPagoApp -> apiAplicacion "Gestiona los pagos de las recompensas usando"
        apiAplicacion -> serviciosAmazon "Uso del balanceador de carga para las petiones HTTPS / Alojamineto usando s3 bucket aws - alojamiento de API usando"
        appUsuario -> tiendaAplicaciones "Alojamineto de la aplicacion para descarga de los ususarios"

        /*Component level connections */
        appUsuario -> apiSeguridad "Realiza llamada API [Json/Https]"
        apiSeguridad -> apiRegistro "Realiza llamada API [Json/Https]"
        apiSeguridad -> apiPublicaciones "Realiza llamada API [Json/Https]"
        apiSeguridad -> apiListaMascostas "Realiza llamada API [Json/Https]"
        apiSeguridad -> apiForoMascotas "Realiza llamada API [Json/Https]"
        apiSeguridad -> apiIntegacionPasarelasPagos "Realiza llamada API [Json/Https]"
        apiSeguridad -> apiIntegacionRedesSociales "Realiza llamada API [Json/Https]"
        apiSeguridad -> apiIntegacionAmazonS3Bucket "Realiza llamada API [Json/Https]"
        apiRegistro -> apiOrm "Realiza transaciones sql"
        apiPublicaciones -> apiOrm "Realiza transaciones sql"
        apiListaMascostas -> apiOrm "Realiza transaciones sql"
        apiForoMascotas -> apiOrm "Realiza transaciones sql"
        apiOrm -> baseDatos "Lee y escribe [SqlServer/TCP]"
        apiIntegacionPasarelasPagos -> apiOrm "Registra las transaciones"
        apiIntegacionAmazonS3Bucket -> apiOrm "Registra las transaciones de nombre y ubicacion de los archivos"
        apiIntegacionPasarelasPagos -> pasarelasPagoApp "Ejecutas llamadas API a [Https]"
        apiIntegacionRedesSociales -> apiRedesSolialesApp "Ejecutas llamadas API a [Https]"
        apiIntegacionAmazonS3Bucket -> serviciosAmazon "Ejecutas llamadas API a [Https]"


    }
    
   

    views {
        systemContext appSoftwareSystem "SystemContext" "Diagrama de contexto"{
            include *
            autoLayout
        }
        
        container appSoftwareSystem "ContainerLevel" "Diagrama de contenedor" {
            include *
            autoLayout
        }

        component apiAplicacion "APIAplication" "Aplicación API" {
            include *
            autoLayout
        }

        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "ExternalSystem" {
                background #808080
                color #ffffff
            }
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }
            element "Aplication" {
                background #08427b
                shape MobileDevicePortrait
            }
            element "API" {
                background #08427b
            }
            element "DataBase" {
                background #08427b
                shape Cylinder
            }
            
            
        }
    }
    
    
    
}