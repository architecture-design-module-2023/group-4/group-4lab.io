## Usabilidad

**Status**: Propuesto

**Contexto**:
El requerimiento no funcional de usabilidad tiene como objetivo asegurar que el sistema sea comprensible y fácil de usar, incluso para personas que no esten relacionadas con la tecnología. Para lograr esto, es necesario tomar ciertas decisiones en cuanto al diseño y la interacción del sistema.

**Decisiones**:
1. Diseño intuitivo: Se tomará la decisión de utilizar un diseño de interfaz de usuario intuitivo que siga los estándares y convenciones de diseño establecidos siguiendo la guía de "Human Interface guideline" para iOs y "Guia de Google UX" para Android. Obteniendo un diseño organizado y de facil navegación.

2. Flexibilidad y personalización: Se incluirá opciones para ajustar el tamaño de fuente, la configuración de colores, esto permitirá personalizar la aplicación a gusto y necesidad del usuario. 

3. Retroalimentación visual: Se implementará una retroalimentación visual para informar a los usuarios sobre el estado de las acciones que realicen. Se utilizarán indicadores visuales, como cambios de color, animaciones o mensajes emergentes, para proporcionar retroalimentación inmediata y comprensible.

4. Ayuda y documentación: Se proporcionará ayuda y documentación accesible, clara y concisa para guiar a los usuarios en el uso del sistema. Esto puede incluir tutoriales interactivos, documentación en línea o asistentes contextuales que brinden información relevante en el momento adecuado.


**Consecuencias**:
- Mejora de la experiencia del usuario: Estas decisiones contribuirán a mejorar la usabilidad del sistema, facilitando su uso y comprensión por parte de los usuarios finales.
- Aumento de la satisfacción del usuario: Al hacer el sistema más intuitivo y personalizable, se espera que los usuarios se sientan más satisfechos con su experiencia de uso.
- Reducción de errores y problemas de aprendizaje: La retroalimentación visual adecuada y la ayuda documentada ayudarán a los usuarios a evitar errores y a aprender a utilizar el sistema de manera eficiente.
- Posible aumento en los costos de desarrollo: La implementación de características adicionales de usabilidad puede requerir más recursos y tiempo de desarrollo, lo que puede aumentar los costos asociados.
- Posible complejidad adicional: Al permitir flexibilidad y personalización, existe el riesgo de agregar complejidad al sistema, lo que puede dificultar su mantenimiento y escalabilidad.

