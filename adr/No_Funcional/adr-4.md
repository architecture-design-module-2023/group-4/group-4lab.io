# Consumo de recursos

## Status

Propuesto

## Contexto
<div style="text-align: justify">

Para realizar la publicacion de un caso como lo es la perdida de una mascota, se debe tener un máximo de caracteres y peso de la imagen que se subiría en la publicación.
<div/>

## Decisión
<div style="text-align: justify">

Tener un máximo de caracteres específicos para todas las publicaciones realizadas en la aplicación el cual nos ayuda a tener una mayor cantidad de publicaciones. Al optimizar el consumo de recurso, la aplicación estará mejor preparada para manejar un mayor volumen de datos y usuarios, lo que garantiza un funcionamiento suave y confiable incluso en momentos de alta demanda. Al regular el consumo de recursos se puede controlar la cantidad de datos que utiliza la aplicación, lo que especialmente relevante en areas donde el internet es limitado o costoso esto pude marcar la diferencia en la recuperación de la mascota.
<div/>

## Consecuencias

- Obtener un mayor rendimiento del dispositivo.
- Optimizar el ahorro de batería.
- Evitar el sobrecalentamiento del dispositivo
- Minimizar el uso de datos y costos adicionales.

## Fecha última actualización
24-Junio-2023