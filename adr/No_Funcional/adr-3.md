# Tiempo de Respuesta

## Estatus

Propuesto

## Contexto

La aplicación debe responder a las interacciones del usuario de manera rapida, los mensajes deben ser instantáneos al encontrar mascotas perdidas agilitando la búsqueda de la mascota perdida.

## Decisiones

Se toma como decisión utilizar DevTools de Flutter, Flutter performance Monitor, Android Studio para aplicaciones Android y Xcode para aplicaciones iOS, que son herramientas integradas para medir el rendimiento de la aplicación.

## Consecuencias

- Abandono de la apliacion, los usuarios tienden a ser impacientes.
- Experiencia de usuario insatisfactoria y frustación.
- Pérdida de oportunidades comerciales.

## Fecha Última Actualización
24-junio-2023