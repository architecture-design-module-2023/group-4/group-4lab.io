# Compatibilidad con android y ios

## Estatus

Propuesto

## Contexto

iOS y Android son los dos sistemas operativos móviles dominantes, con una participación significativa del mercado global. Al desarrollar una aplicación que sea compatible con ambas plataformas nos enfocamos a un público más amplio y maximizamos su alcance de mercado.

## Decisiones

Se propone desarrollar la aplicación utilizando el framework Flutter. Flutter es un framework de código abierto desarrollado por Google que permite crear aplicaciones nativas para dispositivos móviles (iOS y Android) y web desde un solo código base.

## Consecuencias

Con Flutter, se puede escribir una vez y ejecutar la aplicación en múltiples plataformas. Esto significa que no es necesario desarrollar y mantener dos bases de código separadas para iOS y Android, lo que ahorra tiempo, esfuerzo y dinero.
Además, Flutter compila directamente en código nativo, lo que resulta en una mayor velocidad y rendimiento de la aplicación.

## Fecha última actualización
24-Junio-2023