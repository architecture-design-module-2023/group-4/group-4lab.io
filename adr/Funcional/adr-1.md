# Registro de usuarios

## Status

Propuesto

## Contexto
<div style="text-align: justify">
La aplicación proporcionará un registro de usuarios por medio del cual se pueda recopilar información de los usuarios para de esta forma tener una mayor base de usuarios por medio del cual los usuarios se puedan comunicar.
<div/>

## Decisión
<div style="text-align: justify">

Con el fin de obtener la mayor cantidad de información necesaria de los usuarios registrados en la aplicación se propone el crear una interfaz que permita a los usuarios registrarse.

El registro de usuario tendra los siguientes requisitos:
- Nombre: de la persona a registrarse
- Correo electrónico: de la persona que se registra
- Dirección: de la persona a registrarse
- Teléfonos: de la persona para facilitar la comunicación de los usuarios
- Identificación: número de cédula o pasaporte la persona
- Foto de perfil: de la persona
- Ciudad: lugar donde vive la persona
- Información adicional : aqui se detallan datos adicionales de la persona a registrarse

<div/>

## Consecuencias

- Las personas que no estan registradas o no cuentan con la suficiente información dentro de la aplicación podrían tener problemas al momento de buscar sus mascotas o querer reportar mascotas encontradas lo que causaría que los casos no se cierren.
- Limitar la capacidad de la aplicación para facilitar la reunión de mascotas perdidas con sus propietarios


## Fecha última actualización
11-Julio-2023