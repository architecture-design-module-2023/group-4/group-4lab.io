# Comentarios en entradas de mascotas desaparecidas

## Estatus

Propuesto

## Contexto

La aplicación proporcionará una función para comentar en las publicaciones de mascotas desaparecidas que permita a los usuarios proporcionar detalles de la descripción de la mascota encontrada.

## Decisiones

Se propone desarrollar una interfaz de usuario que permita comentar sobre las publicaciones de las mascotas perdidas.
Las publicaciones incluirán la siguiente información:
-	Descripción de la mascota encontrada
-	ciudad 
-	fecha y hora
-	foto
-	área aproximada donde se la vió

## Consecuencias

-   la mascota puede ya no encontrarse en la ubicación vista por ultima vez.
-   puden existir muchos comentarios que no aporten información relevante sobre la ubicación de la mascota perdida.

## Fecha última actualización
10-Julio-2023



