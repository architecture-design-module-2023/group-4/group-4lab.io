# ADR - Requerimiento funcional de listado de publicaciones de mascotas perdidas

## Status
Aceptado

## Contexto
En el servicio de búsqueda de mascotas perdidas, es necesario que cualquier persona pueda ver una lista de mascotas desaparecidas cerca de su ubicación. La lista debe proporcionar resúmenes de las mascotas, incluyendo su nombre, raza y una imagen destacada. Esta lista se generará en función de los datos de ubicación proporcionados por los usuarios al crear las publicaciones. Es necesario organizar y estructurar la información para difundir de manera más amplia la lista de mascotas y proporcionar a los usuarios un lugar centralizado donde puedan realizar sus búsquedas. Además, se requiere que la lista incluya filtros de búsqueda por fecha, ciudad y tipo de mascota, así como la capacidad de realizar búsquedas específicas utilizando palabras clave o características específicas de las mascotas.

## Decision
Se implementará un listado de publicaciones de mascotas perdidas que cumplirá con los siguientes requisitos:
- Mostrará resúmenes de las mascotas, incluyendo su nombre, raza y una imagen destacada.
- Se generará en función de los datos de ubicación proporcionados por los usuarios al crear las publicaciones.
- Permitirá la difusión amplia de la lista de mascotas y ofrecerá un lugar centralizado para que los usuarios realicen sus búsquedas.
- Incluirá filtros de búsqueda por fecha, ciudad y tipo de mascota.
- Permitirá búsquedas específicas utilizando palabras clave o características de las mascotas.

## Consecuencias
- Incrementará la eficiencia en la difusión de información y la búsqueda de mascotas perdidas.
- Mejorará la visibilidad y accesibilidad de las publicaciones de mascotas perdidas.
- Permitirá a los usuarios buscar de manera más efectiva utilizando filtros y palabras clave.
- Facilitará a los usuarios encontrar mascotas desaparecidas cerca de su ubicación.

