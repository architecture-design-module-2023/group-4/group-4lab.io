# Publicación de mascota perdida

## Estatus

Propuesto

## Contexto

La aplicación proporcionará una función que permita a los usuarios crear publicaciones que contengan información relevante sobre mascotas desaparecidas, facilitando el proceso de búsqueda. 

## Decisiones

Se propone desarrollar una interfaz de usuario que permita crear publicaciones sobre mascotas perdidas.
Las publicaciones incluirán la siguiente información:
•	Nombre de la mascota: El nombre de la mascota desaparecida para fines de identificación.
•	Especie: De la mascota desaparecida 
•	Raza: La raza o mezcla de la mascota desaparecida, si se conoce.
•	Tamaño: De la mascota desaparecida
•	Color: De la mascota desaparecida
•	Descripción: Una descripción detallada de la mascota, cualquier característica única.
•	Fecha y hora: La fecha y hora en que la mascota fue vista por última vez o desapareció.
•	Fotos: La opción para que los usuarios carguen una o más fotos claras y recientes de la mascota desaparecida.
•	Área aproximada: El área o lugar donde la mascota fue vista por última vez o se sospecha que se perdió (Coordenadas GPS).

## Consecuencias

Las personas que han visto una mascota perdida en su área podrán revisar estas publicaciones y brindar asistencia.

## Fecha última actualización
10-Julio-2023



